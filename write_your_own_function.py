from math import *

def write_your_function():

    import time

    print("--------------------------")
    print("Write Your Own Function")
    print("(pow(2,3)+sqrt(144))/2+15")
    print("--------------------------")

    #get input
    try:
        userinput0 = str(input("Input: "))
        print("--------------------------")
        if type(eval(userinput0, globals())/1) == float:
            print("{0} = {1}".format(userinput0,eval(userinput0, globals())))
            time.sleep(3)
            write_your_function()
        else:
            print("--------------------------")
            print("Input Error.")
            time.sleep(3)
            write_your_function()
    except:
        write_your_function()
    
write_your_function()
